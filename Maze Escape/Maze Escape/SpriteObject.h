#pragma once
#include <SFML/Graphics.hpp>
class SpriteObject
{
public:
	// Constructors / Destructors
	SpriteObject(sf::Texture& newTexture);
	// Functions
	void DrawTo(sf::RenderTarget& target);
protected:
	// Data
	sf::Sprite sprite;
};