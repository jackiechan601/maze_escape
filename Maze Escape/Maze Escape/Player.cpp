#include "Player.h"
#include "AssetManager.h"
Player::Player(sf::Vector2f startingPos)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/ PlayerStatic.png"), 100, 100, 5.0f)	, velocity(0.0f, 0.0f)
	, speed(300.0f){
	sprite.setPosition(startingPos);

	AddClip("WalkDown", 0, 3);
	AddClip("WalkRight", 4, 7);
	AddClip("WalkLeft", 8, 11);
	AddClip("WalkUp", 12, 15);

	PlayClip("WalkDown");
}void Player::Input()
{
	// Player keybind input
	// Start by zeroing out player velocity
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	bool hasInput = false;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
		PlayClip("WalkUp");
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
		if (!hasInput)
		{
			PlayClip("WalkLeft");
		}
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
		if (!hasInput)
		{
			PlayClip("WalkDown");
		}
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
		if (!hasInput)
		{
			PlayClip("WalkRight");
		}
		hasInput = true;
	}

	if (!hasInput)
	{
		Stop();
	}
}
void Player::Update(sf::Time frameTime)
{
	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	
	// Move the player to the new position
	sprite.setPosition(newPosition);

	AnimatingObject::Update(frameTime);
}