#pragma once
#include "AnimatingObject.h"
class Player : public AnimatingObject
{
public:
	Player(sf::Vector2f startingPos);
	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
private:
	// Data
	sf::Vector2f velocity;
	float speed;
};

