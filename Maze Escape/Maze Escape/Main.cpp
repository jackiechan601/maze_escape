﻿#include <SFML/Graphics.hpp>
#include "AssetManager.h"#include "SpriteObject.h"
#include "Player.h"
int main()
{
	sf::RenderWindow window(sf::VideoMode(1600, 1000), "Maze Escape");

	sf::Clock gameClock;
	sf::Event event;

	Player test(sf::Vector2f(100.0f, 100.0f));

	sf::Time frameTime = gameClock.restart();
	test.Update(frameTime);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		window.clear();
		window.display();
	}
	return 0;
}